class Machine {
    constructor (){
        this.isTurnOn = false;
        this.turnOn = function () {
            console.log('Машина включена');
            this.isTurnOn = true;
        };
        this.turnOff = function () {
            console.log('Машина выключена');
            this.isTurnOn = false;
        };
    }
}
class HomeAppliance extends Machine {
    constructor(){
        super();
        this.isPlugIn = false;
        this.plugIn = function () {
            console.log("Машина подключенна к сети");
            this.isPlugIn = true;
        };
        this.plugOff = function () {
            console.log('Машина отключина от сети');
            this.isPlugIn = false;
        };
        this.turnOn = function () {
            if (this.isPlugIn)  console.log('Машина включена');
        }
    }
}
class WashingMachine extends HomeAppliance {
    constructor(){
        super();
        this.run = function () {
            if (this.isPlugIn && this.isTurnOn) console.log('Машина ЗАПУЩЕНА');
            else console.log("Машина не включена или не подключена к сети");
        }
    }
}
class LightSource extends HomeAppliance {
    constructor(){
        super();
        this.level = 0;
        this.setLevel = function (level) {
            if (this.isPlugIn && (level >= 0 && level <=100)) {
                this.level = level;
                console.log('Уровень освещенности установлен на ' + level);
            }else console.log('Прибор не подключен или выбран не верный уровень освещенности');
        }
    }
}
class AutoVehicle extends Machine {
    constructor(){
        super();
        this.position ={
            x : 0,
            y : 0
        };

        this.setPosition = function (x,y) {
            this.position.x = x;
            this.position.y = y;
            console.log("текущее положение " + this.position.x + " " + this.position.y);
        }
    }
}
class Car extends AutoVehicle {
    constructor(){
        super();
        this.speed = 10;

        this.setSpeed = function (speed) {
            this.speed = speed;
        };

        this.run = function (x,y) {
            let timer = setInterval( () => {
                let currentX;
                let currentY;
                if (x < 0 && y < 0){
                    currentX = this.position.x - this.speed;
                    currentY = this.position.y - this.speed;
                    if (currentX <= x) currentX = x;
                    if (currentY <= y) currentY = y;
                }
                if(x > 0 && y > 0){
                    currentX = this.position.x + this.speed;
                    currentY = this.position.y + this.speed;
                    if (currentX >= x) currentX = x;
                    if (currentY >= y) currentY = y;
                }
                if (x > 0 && y < 0){
                    currentX = this.position.x + this.speed;
                    currentY = this.position.y - this.speed;
                    if (currentX >= x) currentX = x;
                    if (currentY <= y) currentY = y;
                }
                if (x < 0 && y > 0){
                    currentX = this.position.x - this.speed;
                    currentY = this.position.y + this.speed;
                    if (currentX <= x) currentX = x;
                    if (currentY >= y) currentY = y;
                }

                this.setPosition(currentX, currentY);
                if (currentX === x && currentY === y) {
                    clearInterval(timer);
                    console.log("машина в точке назначения");
                }

            },1000);

        }
    }
}